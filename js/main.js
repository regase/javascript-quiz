(function(){
	"use strict";
	var allQuestions = [
		{
			question: "7 + 2 = ?",
			choices: [ "9", "10", "8"],
			correctAnswer: 0
		},
		{
			question: "Java and Javascript is it the same thing?",
			choices: [ "Yes", "No"],
			correctAnswer: 1
		},
		{
			question: "What is 10 + 6 = ?",
			choices: [ "16", "17"],
			correctAnswer: 0
		},
		{
			question: "What is 54 + 6 = ?",
			choices: [ "58", "60", "68", "64"],
			correctAnswer: 1
		},
		{
			question: "What is 30 + 6 = ?",
			choices: [ "38", "36", "34", "30"],
			correctAnswer: 1
		}
	];
	var currentQuestion = 0;
	var yourAnswers = new Object();
	shuffleAnswersToQuestions();

	/**
	 * Shuffles/randomizes the choices
	*/
	function shuffleAnswersToQuestions() {
		for (var i = allQuestions.length - 1; i >= 0; i--) {
			var question = allQuestions[i];
			var correctAns = question.choices[question.correctAnswer];

			question.choices = shuffleArray(allQuestions[i].choices);
			// Find the correct answer again
			for (var i2 = question.choices.length - 1; i2 >= 0; i2--) {
				if (question.choices[i2] == correctAns) {
					question.correctAnswer = i2;
				}
			}
		}
	}

	/**
	 * Saves answers in yourAnswers variable
	*/
	function saveAnswer(index) {
		var val = document.querySelector('[name="answer"]:checked');
		if (val != null) {
			yourAnswers["a" + index] = parseInt(val.value);
		}
	}

	/**
	 * Goes to the next question and if it's the final question it goes to the results screen
	*/
	function nextQuestion() {
		// Save the answer
		saveAnswer(currentQuestion);

		// Check if this was the final question if so show the results
		if (currentQuestion + 1 == allQuestions.length) {
			currentQuestion++;
			showResults();
		}
		else if (currentQuestion < allQuestions.length)
		{
			currentQuestion++;
			showQuestion(currentQuestion);
		}
	}

	/**
	 * Goes to the previous question
	*/
	function previousQuestion() {
		if (currentQuestion > 0 && currentQuestion < allQuestions.length) {
			// Save the answer
			saveAnswer(currentQuestion);
			
			currentQuestion--;
			showQuestion(currentQuestion);
		}
	}

	/**
	 * Shows the question at the index, 
	 * this function is used in both previousQuestion and nextQuestion functions
	*/
	function showQuestion(index) {
		var questionNumberHtml = document.getElementById("question-number");
		var questionHTML = document.getElementById("question");
		var choicesHTML = document.getElementById("choices");

		// Hide the button to go back on the first question
		hideShow(index, 'back');

		// Retrieve answer if you have already answered the question
		var ya = yourAnswers["a" + currentQuestion];

		questionHTML.innerHTML = allQuestions[index]["question"];
		questionNumberHtml.innerHTML = 'Question ' + (index + 1);
		choicesHTML.innerHTML = "";
		
		for (var i = 0; i < allQuestions[index]["choices"] .length; i++) {
			// Make it a label so i can click the div easily
			choicesHTML.innerHTML += '' +
				'<label class="choice">' + 
					'<input type="radio" name="answer"' + (ya == i ? 'checked id="ans"':'') + ' value="' + i + '"/>' +
					'<p>' + allQuestions[index]["choices"][i] + '</p>'
				'</label>';
		}
		var val = document.querySelector('[name="answer"]:checked');
		if (val != null) {
			val.focus();
		}
		else {
			document.getElementsByName("answer")[0].focus();
		}
	}

	/**
	 * Shows how many questions you got right
	*/
	function showResults() {
		var questionNumberHtml = document.getElementById("question-number");
		var questionHTML = document.getElementById("question");
		var choicesHTML = document.getElementById("choices");
		var correct = 0;
		document.getElementById("buttons").remove(); // Remove buttons so you can't go back

		for (var i = allQuestions.length - 1; i >= 0; i--) {
			if (allQuestions[i]["correctAnswer"] == yourAnswers["a" + i]) {
				correct++;
			}
		}


		questionNumberHtml.innerHTML = "Results";
		questionHTML.innerHTML = "You answered " + correct + " out of " + allQuestions.length + " correctly.";
		choicesHTML.innerHTML = "";
	}

	/**
	 * Randomize array
	 * Pretty basic shuffle function i took from stackoverflow
	*/
	function shuffleArray(array) {
	    for (var i = array.length - 1; i > 0; i--) {
	        var j = Math.floor(Math.random() * (i + 1)); // get random value from 0 to i
	        var temp = array[i]; // temporarily store a element from the array
	        // Then swap the array[i] and array[j]
	        array[i] = array[j];
	        array[j] = temp;
	    }
	    return array;
	}

	/**
	 * Just a little helper function to hide a element
	*/
	function hideShow(index, elementId) {
		if (index == 0) {
			document.getElementById(elementId).style.visibility = 'hidden';
		}
		else if (index == 1)
		{
			document.getElementById(elementId).style.visibility = 'visible';
		}
	}

	/**
	 * Catch keyboard events
	*/
	function onKey(e) {
	    e = e || window.event;

	    if (e.keyCode == '37') {
	    	previousQuestion();
	    	e.stopPropagation();
	    	e.preventDefault();
	    }
	    else if (e.keyCode == '39') {
	    	nextQuestion();
	    	e.stopPropagation();
	    	e.preventDefault();
	    }
	    else if (e.keyCode == '13') {
	    	nextQuestion();
	    }
	}

	// Show the first question
	showQuestion(0);
    
    // Button events
	document.getElementById("next").onclick = nextQuestion;
	document.getElementById("back").onclick = previousQuestion;
	document.onkeydown = onKey;

})();