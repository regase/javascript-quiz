(function(){
	"use strict";

	/**
	 * App 'class' is like a starter function
	*/
	function App ()
	{
		var questions = [
			{
				question: "Who is the current president of the United States",
				choices: [ "Barack Obama", "Donald Trump", "George W. Bush", "Bill Clinton"],
				correctAnswer: 0
			},
			{
				question: "What is 7 + 2 = ?",
				choices: [ "8", "9", "2", "10"],
				correctAnswer: 1
			},
			{
				question: "What is 7 + 2 = ?",
				choices: [ "8", "9", "2", "10"],
				correctAnswer: 1
			},
			{
				question: "What is 7 + 2 = ?",
				choices: [ "8", "9", "2", "10"],
				correctAnswer: 1
			}
		];

		/* Quiz setup */
		var user = new User();
		var quiz = new Quiz("Math Quiz", "This is quiz 1", questions, user);

		this.quizList = [quiz];
		this.currentQuiz = quiz;
		this.currentQuiz.showQuestion(0); // Show the first question of the current quiz

		user.showCurrentScore();
	}

	/**
	 * Quiz 'class' stores the question as a list of Question objects
	*/
	function Quiz(name, description, questions, user) {
		this.name = name;
		this.description = description;
		this.questions = questions;
		this.currentQuestion = questions[0];
		this.currentQuestionIndex = 0;
		this.user = user;
	}

	Quiz.prototype = {
		constructor: Quiz,
		showQuestion: function (index) {
			this.currentQuestionIndex = index;
			this.currentQuestion = this.questions[this.currentQuestionIndex];
			$(".main-container").html(
				'<h1 class="text-center"> Question ' + (index + 1) + '</h1>' +
				'<h3 class="text-center">' + this.currentQuestion.question + '</h3>' + 
				'<div id="choices"></div>'
			);
			this.currentQuestion.choices.forEach(function(choice, index) {
				$("#choices").append(
					'<div class="box col-xs-6 col-sm-6 col-md-6">' +
						'<input type="radio" id="asm' + index + '" name="asm" value="' + index + '">' + 
						'<label for="asm' + index + '">' + 
							'<div class="btn btn-primary">' + choice + '</div>' +
						'</label>' +
					'</div>'
				);
			});
			document.addEventListener("keydown", this, false);
			$('input[type="radio"]').click({quiz: this},function(e){
			    if ($(this).is(':checked'))
			    {
			    	e.data.quiz.answerQuestion($(this).val());
			    }
		  	});
		},
		nextQuestion: function () {
			if (this.currentQuestionIndex + 1 < this.questions.length) {
				this.showQuestion(this.currentQuestionIndex + 1);
			}
			else
			{
				this.showFinalScreen();
			}
			$(".main-container").toggle("slide", { direction: "left" }, 300);
		},
		answerQuestion: function (answer) {
			this.user.answers["a" + this.currentQuestionIndex] = answer;
			var percentage = parseInt((100 / this.questions.length) * (this.currentQuestionIndex + 1)) + "%";
			var per = 100 / this.questions.length;
			if (answer == this.currentQuestion.correctAnswer) {
				// Your answer was correct
				$(".progress").append('<div class="progress-bar progress-bar-success" role="progressbar" style="width:'+per+'%"></div>');

				// Give points for a correct answer
				this.user.currentScore += 150;
				this.user.showCurrentScore();
			}
			else {
				// Answer incorrect
				$(".progress").append('<div class="progress-bar progress-bar-danger" role="progressbar" style="width:'+per+'%"></div>');
				$(".progress").effect("bounce", "slow");
			}
			// create a refrence to this
			var ref = this;
			$(".main-container").toggle("slide", {
				direction: "right", 
				complete: function () {
					ref.nextQuestion();
				}
			}, 300);
		},
		showUpdateScoreboard: function() {
			var users = JSON.parse(localStorage.getItem("users"));
			// Sort by score
			users.sort(function(a, b) {
				return parseFloat(b.currentScore) - parseFloat(a.currentScore);
			});
			$(".scoreboard").empty();
			users.forEach(function(user, index) {
				$(".scoreboard").append(
					'<ul>' + user.name + ": " + user.currentScore + '</ul>'
				);
			});
		},
		showFinalScreen: function () {
			// After last question
			$(".main-container").html(
				'<button class="btn btn-primary col-xs-12 col-md-6 col-md-offset-3" id="play-again">Play Again</button></br></br>' +
				'<h3 class="text-center"> Your Score: ' + this.user.currentScore + '</h3>' +
				'<h3 class="text-center">Submit your score to the scoreboard </h3>' +
				'<div class="col-md-6 col-md-offset-3 form-group">' +
					'<label for="name">Name</label>' +
					'<input type="text" class="form-control" id="name" placeholder="Name">' + 
				'</div>' + 
				'<button class="btn btn-primary col-xs-12 col-md-6 col-md-offset-3" id="submit-score">Submit</button>' +
				'<div class="sc col-xs-12 col-md-6 col-md-offset-3">' +
					'<h3> Scoreboard </h3>' +
					'<div class="scoreboard"></div>' +
				'</div>'
			);
			this.showUpdateScoreboard();

			/* Button actions */
			$("#play-again").click($.proxy(this.playAgain, this));
			$("#submit-score").click($.proxy(this.submitScore, this));
		},
		playAgain: function () {
			$(".progress").empty();
			this.user.currentScore = 0;
			this.user.showCurrentScore();
			this.showQuestion(0);
		},
		submitScore: function () {
			var name = $("#name").val();
			this.user.name = name;

			if (!localStorage.getItem("users")) {
				localStorage.setItem("users", JSON.stringify([this.user]));
			}
			else {
				var users = JSON.parse(localStorage.getItem("users"));
				users.push(this.user);
				localStorage.setItem("users", JSON.stringify(users));
			}
			$("#submit-score").remove();
			$("#name").prop('disabled', true);
			this.showUpdateScoreboard();
		},
		handleEvent: function(e) {
			// verify that there's a handler for the event type, and invoke it
			return this[e.type] && this[e.type](e);
    	},
		keydown: function (e) {
			e = e || window.event;

			if (e.keyCode == '37' || e.keyCode == '38' || e.keyCode == '40' || e.keyCode == '39') {
	    		e.stopPropagation();
	    		e.preventDefault();
		    }
			else if (e.keyCode == '13') {
				$(".main-container").effect("shake", {  }, 200);
			}
		}
	}

	/**
	 * User 'class' stores the score,name, etc
	*/
	function User() {
		this.name = "";
		this.currentScore = 0;
		this.answers = new Object();
	}

	User.prototype =  {
		constructor: User,
		showCurrentScore: function() {
			$(".current-score").html("Score: " + this.currentScore);	
		}
	}

	/**
	 * Question 'class' stores the choices and answer to the question
	*/
	function Question (question, choices, correctAnswer) {
		this.question = question;
		this.choices = choices;
		this.correctAnswer = correctAnswer;
	}

	Question.prototype = {
		constructor: Question,
		showQuestion: function() {

		}
	}

	var app = new App();
})();